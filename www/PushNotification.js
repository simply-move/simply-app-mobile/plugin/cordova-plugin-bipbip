var PushNotifLoginEnabler = function(){};

PushNotifLoginEnabler.prototype.appId = null;
PushNotifLoginEnabler.prototype.senderId = null;
PushNotifLoginEnabler.prototype.login = localStorage.getItem("PushNotifiactionLoginEnabler-Login") ? localStorage.getItem("PushNotifiactionLoginEnabler-Login") : "anonymous";
PushNotifLoginEnabler.prototype.token = null;
PushNotifLoginEnabler.prototype.registred = localStorage.getItem("PushNotifiactionLoginEnabler-Registered") ? localStorage.getItem("PushNotifiactionLoginEnabler-Registered") : false;
PushNotifLoginEnabler.prototype.customAction = null;;
PushNotifLoginEnabler.prototype.enablerIp = "http://192.168.18.24:/push";
PushNotifLoginEnabler.prototype.basicAuth = null;
PushNotifLoginEnabler.prototype.targetMode = "dev";
PushNotifLoginEnabler.prototype.debug = false;
PushNotifLoginEnabler.prototype.pushEnabler = null;
PushNotifLoginEnabler.prototype.unsuscribeTimoutHack = null;
PushNotifLoginEnabler.prototype.registrationCallback = null;


PushNotifLoginEnabler.prototype.WSLoginRegistrationUrl = "/web/subscribeByPushId";
PushNotifLoginEnabler.prototype.WSLoginUnRedistrationUrl = "";

PushNotifLoginEnabler.prototype.initPushNotification = function(appID,senderID,config){
    
    cordova.plugins.certificates.trustUnsecureCerts(true);

    this.appId = appID;
    this.senderId = senderID;

    if (typeof config !== 'undefined'){
        if (typeof config.enablerIp !== 'undefined')
            this.enablerIp = config.enablerIp;
        if (typeof config.basicAutentification !== 'undefined')
            this.basicAuth = config.basicAutentification;
        if (typeof config.customAction !== 'undefined')
            this.customAction = config.customAction;
        if (typeof config.targetMode !== 'undefined')
            this.targetMode = config.targetMode;
        if (typeof config.debug !== 'undefined')
            this.debug = config.debug;
        if (typeof config.registrationCallback !== 'undefined')
            this.registrationCallback = config.registrationCallback;
    }

    //this.unsuscribeTimoutHack = setTimeout(window.plugins.pushNotificationLoginEnabler.unSubscribeNotification, 10000);

    this.appendLog('registering ' + device.platform);

    this.pushEnabler = PushNotification.init({ "android": {"senderID": this.senderId}, "ios": {"alert": "true", "badge": "true", "sound": "true"}, "windows": {} } );

    this.pushEnabler.on('registration', function(data) {
        // data.registrationId
        //window.clearTimeout(window.plugins.pushNotificationLoginEnabler.unsuscribeTimoutHack);
        window.plugins.pushNotificationLoginEnabler.token = data.registrationId;
        window.plugins.pushNotificationLoginEnabler.subscribeForLoginNotification();
        if (window.plugins.pushNotificationLoginEnabler.registrationCallback) 
            window.plugins.pushNotificationLoginEnabler.registrationCallback.call(window,data.registrationId);
    });

    this.pushEnabler.on('notification', function(data) {
        // data.message,
        // data.title,
        // data.count,
        // data.sound,
        // data.image,
        // data.additionalData
        var event = [];
        if (typeof data.texte != 'undefined')
            event.texte = data.texte;
        if (typeof data.message != 'undefined')
            event.texte = data.message;
        event.data = data.additionalData.data;
        event.foreground = data.additionalData.foreground;
        event.title = data.title;
        event.count = data.count;
        event.image = data.image;
        event.sound = data.sound;

        window.plugins.pushNotificationLoginEnabler.execAction(event,null);

    });

    this.pushEnabler.on('error', function(e) {
        // e.message
        window.plugins.pushNotificationLoginEnabler.appendLog('Push Notif Registration failed: ' + e.message);
    });

};

PushNotifLoginEnabler.prototype.getStatus = function(){
    return this.registered;
};


PushNotifLoginEnabler.prototype.appendLog = function(string){
    if (this.debug)
        console.log("[PushNotification Login Enabler LOG]" + string);
};
  

PushNotifLoginEnabler.prototype.setDebug = function(debugStatus){
    this.debug = debugStatus;
};


PushNotifLoginEnabler.prototype.setBadgeNumber = function(count){
    this.pushEnabler.setApplicationIconBadgeNumber(
            function(){console.log("[PushNotification reset setBadge LOG] SUCCES");}, 
            function(){console.log("[PushNotification reset setBadge LOG] FAILED");}, 
    count); 
};

PushNotifLoginEnabler.prototype.resetBadgeNumber = function(){
    this.setBadgeNumber(0); 
};

PushNotifLoginEnabler.prototype.setPushIds = function(newLlogin,callback){

    if (newLlogin.constructor !== Array){
        newLlogin = new Array(newLlogin);
    }
    if (newLlogin == this.login && this.registered) return;
    window.plugins.pushNotificationLoginEnabler.login = newLlogin;
    window.plugins.pushNotificationLoginEnabler.registered = false;
    window.plugins.pushNotificationLoginEnabler.storeData();
    var _callback = window.plugins.pushNotificationLoginEnabler.callback || function(){};
    this.subscribeForLoginNotification(_callback);

};

PushNotifLoginEnabler.prototype.setLogin = function(newLlogin,callback){

    PushNotifLoginEnabler.prototype.setPushIds(newLlogin,callback);

};

PushNotifLoginEnabler.prototype.getPlatformID = function(){
    if (navigator.userAgent.match(/Windows/i))
        return "WINDOWS";
    if (navigator.userAgent.match(/Android/i))
        return "ANDROID";
    if ((navigator.userAgent.match(/iPad/i)) || (navigator.userAgent.match(/iPhone/i)))
        return "IOS";

    this.appendLog('Login UnRegister MobiAppsEnabler: error unknown platform');
    return "UNKNOWN";
};

PushNotifLoginEnabler.prototype.updateLoginToken = function(login,callback){
    if (window.plugins.pushNotificationLoginEnabler.token == null) return;
    var _callback = callback || function(){};
    var _url = window.plugins.pushNotificationLoginEnabler.enablerIp + window.plugins.pushNotificationLoginEnabler.WSLoginRegistrationUrl;
    var _params = "?token=" + window.plugins.pushNotificationLoginEnabler.getPlatformID() + "|" + window.plugins.pushNotificationLoginEnabler.appId + "|" + window.plugins.pushNotificationLoginEnabler.token + "&target_mode=" + window.plugins.pushNotificationLoginEnabler.targetMode;

     if (login.constructor === Array){
        for (var i = login.length - 1; i >= 0; i--) {
            _params += "&pushId=" + login[i];
        };
    }else{
        _params += "&pushId=" + login;
    }

    
    
    window.plugins.pushNotificationLoginEnabler.appendLog('Login Registration MobiAppsEnabler -> Request : ' + _url + _params);
    var _resultCallback = function(data, status){
        window.plugins.pushNotificationLoginEnabler.appendLog('Login Registration MobiAppsEnabler -> Response : ' + data);
        if (typeof callback !== 'undefined')
            callback.call(window,status);
    }

    window.plugins.pushNotificationLoginEnabler.doAjaxRequest(_url + _params,_resultCallback,"POST",window.plugins.pushNotificationLoginEnabler.basicAuth);
};

PushNotifLoginEnabler.prototype.subscribeForLoginNotification = function(callback){ 
    var _callback = callback || function(){};
    window.plugins.pushNotificationLoginEnabler.updateLoginToken(window.plugins.pushNotificationLoginEnabler.login,_callback);
    window.plugins.pushNotificationLoginEnabler.storeData();
};


PushNotifLoginEnabler.prototype.unSubscribeNotification = function(){
    if ( typeof window.plugins.pushNotificationLoginEnabler.registered == 'undefined' || window.plugins.pushNotificationLoginEnabler.registered == false) return;
    window.plugins.pushNotificationLoginEnabler.appendLog('Login UnRegister MobiAppsEnabler');
    window.plugins.pushNotificationLoginEnabler.updateLoginToken("anonymous");
    window.plugins.pushNotificationLoginEnabler.registered = false;  
    window.plugins.pushNotificationLoginEnabler.login = null;
    window.plugins.pushNotificationLoginEnabler.storeData();
    window.plugins.pushNotificationLoginEnabler.resetBadgeNumber();
};

PushNotifLoginEnabler.prototype.execAction = function(payload){
   
    window.plugins.pushNotificationLoginEnabler.customAction.call(window,payload);

};

/**
 * Effectue une requete via un appel AJAX.
 * @param url URL
 * @param callback Fonction callback.
 * @param method Methode HTTP (par defaut 'get').
 * @param basicAutentification string acces key.
 */
PushNotifLoginEnabler.prototype.doAjaxRequest = function(url, callback, method, basicAutentification) {
    var headers = {};
    if (typeof basicAutentification !== 'undefined'){
        headers['Authorization'] = basicAutentification;
    }
        
    callback = callback || function(){};
    data =  {};
    method = method || 'GET';
    dataType =  'xml';
    contentType = 'application/x-www-form-urlencoded; charset=UTF-8';

    var xhr = new XMLHttpRequest(); 
    
    xhr.open(method,url,true);
    xhr.setRequestHeader("Content-type",contentType);
    xhr.send();

    xhr.onreadystatechange = function() {
        if (xhr.status == 200 || xhr.status == 0) {
            window.plugins.pushNotificationLoginEnabler.appendLog('[PushNotification] Login Register MobiAppsEnabler succes');
            window.plugins.pushNotificationLoginEnabler.registered = true;
            window.plugins.pushNotificationLoginEnabler.storeData();
            callback.call(window, xhr.data, 1);
        }else{
            window.plugins.pushNotificationLoginEnabler.appendLog('[PushNotification] Failed to register MobiAppsEnabler');
            window.plugins.pushNotificationLoginEnabler.registered = false;  
            window.plugins.pushNotificationLoginEnabler.login = null;
            window.plugins.pushNotificationLoginEnabler.storeData();
            callback.call(window, xhr.data, 0);
        }
    };

};

PushNotifLoginEnabler.prototype.storeData = function(){
    localStorage.setItem("PushNotifiactionLoginEnabler-Login", this.login);
    localStorage.setItem("PushNotifiactionLoginEnabler-Registered", this.registered);   
};

//-------------------------------------------------------------------

if(!window.plugins) {
    window.plugins = {};
}

if (!window.plugins.pushNotificationLoginEnabler) {
    window.plugins.pushNotificationLoginEnabler = new PushNotifLoginEnabler();
}

