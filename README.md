# mobiapps-phonegap-plugin-push 

> Register and receive push notifications via mobiapps prizzly enabler

## Installation

This requires phonegap/cordova CLI 4.0+ 

```
cordova plugin add https://git.mobiapps.fr/bip-bip/cordova-plugin.git
```

## Supported Platforms

- Android
- iOS
- Windows Universal (Not Yet)

## Quick Example

```
// deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
        
        var _options = {};
        _options["customAction"] = app.doActionForNotification;
        _options["targetMode"] = "prod";
        _options["debug"] = true;
        _options["enablerIp"] = "https://customers.mobiapps.fr/services/push-notif";

        
        window.plugins.pushNotificationLoginEnabler.initPushNotification("fr.mobiapps.sample","618109242883",_options);
    },

```

## Init

#### window.plugins.pushNotificationLoginEnabler.initPushNotification(applicationId,senderId,options)

Parameter | Description
--------- | ------------
`applicationId` | `JSON Object` platform specific initialization options.
`senderId` | `String` Maps to the project number in the Google Developer Console.
`options` | `Object` Optional. 
`options.customAction` | `Function` Optional. Sets the callback for incoming notifications.
`options.targetMode` | `prod/dev`  Override platform scheme.
`options.debug` | `Boolean`  Display logs.
`options.enablerIp` | `String`  Specify Mobiapps enabler Url.



## options.customAction callBack 

### Quick Example

```
    doActionForNotification: function(payload){
        alert('Notification recieved: '+ payload.texte + " Data:"+ payload.data);
        // If you need to have a local notification
        // 
    }

```
## Force badge number

#### window.plugins.pushNotificationLoginEnabler.resetBadgeNumber()

#### window.plugins.pushNotificationLoginEnabler.setBadgeNumber(num)

Parameter | Description
--------- | ------------
`num` | `Integer` the badge value.


### Payload structure

Parameter | Description
--------- | ------------
`text` | `String` Notification message.
`data` | `String` Custom data field.
`foreground` | `Boolean` Was appliaction in background or foreground when notification arrives. 

## Subscribe for notification

### Quick Example

#### simple id

```
	var id = "pierre@toto.fr";
    window.plugins.pushNotificationLoginEnabler.setPushIds(id);

```

#### multiple ids
```
	var ids = ["pierre@toto.fr","test"];
    window.plugins.pushNotificationLoginEnabler.setPushIds(ids);

```


## Unsubscribe all ids

### Quick Example

```
    window.plugins.pushNotificationLoginEnabler.unSubscribeNotification();

```

